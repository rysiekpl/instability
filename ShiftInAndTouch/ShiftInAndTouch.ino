
// Define Connections to 74HC165


// output register pins
int data_pin = 11;
int latch_pin = 8;
int clock_pin = 12;

int fabricRead = A0;

const int NUM_REGISTERS = 9;
const int LEDS_PER_REGISTER = 7;

// bytes to control the LEDS
// you need as many entries here as there are shift registers
byte leds[NUM_REGISTERS];

// input stuff
const int SQUARE0 = A0;
const int SQUARE1 = A1;
const int NUM_SQUARES = 2;

int squares[] = {SQUARE0, SQUARE1};

// nasty global stuff
int square0_count = 0;
int square1_count = 7;

void enable_led(int led) {
  int bank = led / LEDS_PER_REGISTER;
  int position = led % LEDS_PER_REGISTER;

  leds[bank] = bitSet(leds[bank], position);

  Serial.println(bank);
  Serial.println(position);
  Serial.println("---");
}

void disable_led(int led) {
  int bank = led / LEDS_PER_REGISTER;
  int position = led % LEDS_PER_REGISTER;

  leds[bank] = bitClear(leds[bank], position);
}

void clear_leds() {
  for(int i = 0; i < NUM_REGISTERS; i++) {
    leds[i] = 0;
  }
}

void write_leds() {
  digitalWrite(latch_pin, LOW);
  for(int i = NUM_REGISTERS - 1; i >= 0; i--) {
    shiftOut(data_pin, clock_pin, MSBFIRST, leds[i]);
  }
  digitalWrite(latch_pin, HIGH);
}

void setup()
{

  // Setup Serial Monitor
  Serial.begin(9600);

  pinMode(data_pin, OUTPUT);
  pinMode(latch_pin, OUTPUT);
  pinMode(clock_pin, OUTPUT);

  clear_leds();

  for(int i = 0; i < NUM_SQUARES; i++) {
    pinMode(squares[i], INPUT);
  }
}

void loop()
{
  // shift_test();
  // shift_test_advanced();
  input_test();
}

void input_test() {
  if(check_square(SQUARE0) < 500) {
    disable_led(0);
    disable_led(1);
    square0_count++;
    if(square0_count > 1) {
      square0_count = 0;
    }

    enable_led(square0_count);
  }

  if(check_square(SQUARE1) < 500) {
    disable_led(7);
    disable_led(8);
    square1_count++;
    if(square1_count > 8) {
      square1_count = 7;
    }

    enable_led(square1_count);
  }

  write_leds();
  delay(300);
}

void shift_test_advanced() {
  enable_led(0);
  enable_led(1);
  enable_led(7);
  enable_led(8);
  write_leds();
  delay(500);
  disable_led(0);
  disable_led(8);
  write_leds();
  delay(500);
}

void shift_test()
{
  for(int i = 0; i <= 3; i++) {
    for(int j = 0; j <= 3; j++) {
      digitalWrite(latch_pin, LOW);
      shiftOut(data_pin, clock_pin, MSBFIRST, j);
      shiftOut(data_pin, clock_pin, MSBFIRST, i);
      digitalWrite(latch_pin, HIGH);
      delay(500);
    }
  }
}

int check_square(int square)
{
  if(square == A0 || square == A1) {
    return analogRead(square);
  }

  return 0;
}
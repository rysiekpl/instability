// ST_CP pin 12
#define LED_LATCH_PIN 8
// SH_CP pin 11
#define LED_CLOCK_PIN 12
// DS pin 14
#define LED_DATA_PIN  11

#define NUM_REGISTERS      9
#define LEDS_PER_REGISTER  7
#define REGISTER_LEDS_MASK 0b11111110

// shake states for a section
#define SHAKE_NONE    0
#define SHAKE_SHAKING 1
#define SHAKE_WILL    2
#define SHAKE_SHOOK   3


// we have a certain space for these events
#define SHAKES_MAX 80

// how long does a single step take, in ms
#define STEP_DELAY 150

// main event loop delay
#define MAIN_LOOP_DELAY 4

#define MUX1_INPUT A15
#define MUX1_A 48
#define MUX1_B 50
#define MUX1_C 52

#define MUX2_INPUT A14
#define MUX2_A 42
#define MUX2_B 44
#define MUX2_C 46

#define MUX3_INPUT A13
#define MUX3_A 36
#define MUX3_B 38
#define MUX3_C 40


struct section {
  byte leds[NUM_REGISTERS];
  byte neighbours[8];
};

// the map of sections
// 
// containing information on each section's LEDs and neighbours
// each section can have max. 10 LEDs and 10 neighbours
section sections_map[ 34 ] = {
    // section 0
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        1, 5, 7, 255, 255, 255, 255
    },
    // section 1
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b11000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        0, 5, 6, 7, 2, 8, 255, 255
    },
    // section 2
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00100000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        1, 7, 8, 9, 10, 3, 255, 255
    },
    // section 3
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00010000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        2, 9, 10, 11, 12, 4, 255, 255
    },
    // section 4
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00001110, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        3, 11, 12, 255, 255, 255, 255, 255
    },
    // section 5
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        0, 1, 6, 13, 14, 255, 255, 255
    },
    // section 6
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        0, 5, 13, 14, 15, 7, 1, 255
    },
    // section 7
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        1, 6, 14, 15, 16, 8, 2, 255
    },
    // section 8
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        1, 7, 15, 16, 17, 9, 2, 255
    },
    // section 9
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b10000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        2, 8, 16, 17, 18, 10, 3, 255
    },
    // section 10
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b01100000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        3, 2, 9, 17, 18, 19, 11, 255
    },
    // section 11
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00011100, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        3, 10, 18, 19, 20, 12, 4, 255
    },
    // section 12
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        4, 3, 11, 19, 20, 255, 255, 255
    },
    // section 13
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        5, 6, 14, 22, 21, 255, 255, 255
    },
    // section 14
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        5, 13, 21, 22, 23, 15, 7, 6
    },
    // section 15
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000010, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        6, 14, 22, 23, 24, 16, 8, 7
    },
    // section 16
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        7, 8, 15, 23, 24, 25, 17, 9
    },
    // section 17
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b11000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        8, 9, 16, 24, 25, 26, 18, 10
    },
    // section 18
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00111110, 0b10000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        9, 10, 11, 17, 19, 25, 26, 27
    },
    // section 19
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b01111110, 0b11110000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        10, 11, 12, 18, 20, 26, 27, 28
    },
    // section 20
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00001110, 0b11110000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        12, 11, 19, 27, 28, 255, 255, 255
    },
    // section 21
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        13, 14, 22, 29, 30, 255, 255, 255
    },
    // section 22
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        13, 14, 15, 21, 23, 29, 30, 255
    },
    // section 23
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00001100, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        14, 15, 16, 22, 24, 30, 31, 255
    },
    // section 24
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        15, 16, 17, 23, 25, 30, 31, 255
    },
    // section 25
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000010, 0b11100000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        16, 17, 18, 24, 26, 31, 32, 255
    },
    // section 26
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00011110, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        17, 18, 19, 25, 27, 31, 32, 255
    },
    // section 27
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b11110000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        18, 19, 20, 26, 28, 32, 33, 255
    },
    // section 28
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        19, 20, 27, 32, 33, 255, 255, 255
    },
    // section 29
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        21, 22, 32, 255, 255, 255, 255, 255
    },
    // section 30
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00001000, 0b00000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        29, 21, 22, 23, 24, 31, 255, 255
    },
    // section 31
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000110, 0b10000000,
        // neighbouring sections; 255 signifies last one; 8 max.
        30, 23, 24, 25, 26, 32, 255, 255
    },
    // section 32
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b01111100,
        // neighbouring sections; 255 signifies last one; 8 max.
        31, 25, 26, 27, 28, 33, 255, 255
    },
    // section 33
    {
        // LEDs, seven bits per shift register; most significant bit has to stay unset!
        0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, 0b00000010,
        // neighbouring sections; 255 signifies last one; 8 max.
        32, 27, 28, 255, 255, 255, 255, 255
    }
};

// is a given section currently pressed?
bool sections_pressed[ 34 ] = {
    false, false, false, false, false,
    false, false, false, false, false,
    false, false, false, false, false,
    false, false, false, false, false,
    false, false, false, false, false,
    false, false, false, false, false,
    false, false, false, false
};

// a shake struct, with all info on a given propagating shake we might need
struct shake {
    // these are SHAKE_NONE, SHAKE_SHAKING, or SHAKE_SHOOK
    byte section_states[ 34 ] = {
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0, 0,
        0, 0, 0, 0
    };
    // when is the next step to be taken?
    unsigned long next_step = 0;
};

// arrays of planned shakes
shake shakes[SHAKES_MAX];

void setLedState(byte leds_lit[]) {
  // ST_CP LOW to keep LEDs from changing while reading //Serial data
  digitalWrite(LED_LATCH_PIN, LOW);

  // shift out all registers
  for (int i = NUM_REGISTERS - 1; i >= 0; i--) {
    shiftOut(LED_DATA_PIN, LED_CLOCK_PIN, LSBFIRST, leds_lit[i]);
  }

  // ST_CP HIGH change LEDs
  digitalWrite(LED_LATCH_PIN, HIGH);
}

// [multiplexer, A, B, C, A_PIN, B_PIN, C_PIN]
// if multiplexer is -1 then we are using an input pin directly
// in which the entry for A should be the input pin
int section_to_input[34][7] = {
  {MUX1_INPUT, 0, 0, 0, MUX1_A, MUX1_B, MUX1_C}, //pin 0 section 1
  {MUX1_INPUT, 1, 0, 0, MUX1_A, MUX1_B, MUX1_C}, 
  {MUX1_INPUT, 0, 1, 0, MUX1_A, MUX1_B, MUX1_C}, 
  {MUX1_INPUT, 1, 1, 0, MUX1_A, MUX1_B, MUX1_C}, 

  {MUX1_INPUT, 0, 0, 1, MUX1_A, MUX1_B, MUX1_C}, //pin 4 section 5
  {MUX1_INPUT, 1, 0, 1, MUX1_A, MUX1_B, MUX1_C}, 
  {MUX1_INPUT, 0, 1, 1, MUX1_A, MUX1_B, MUX1_C}, 
  {MUX1_INPUT, 1, 1, 1, MUX1_A, MUX1_B, MUX1_C}, 

  {MUX2_INPUT, 0, 0, 0, MUX2_A, MUX2_B, MUX2_C}, //pin 0 section 9
  {MUX2_INPUT, 1, 0, 0, MUX2_A, MUX2_B, MUX2_C}, 
  {MUX2_INPUT, 0, 1, 0, MUX2_A, MUX2_B, MUX2_C}, 
  {MUX2_INPUT, 1, 1, 0, MUX2_A, MUX2_B, MUX2_C},

  {MUX2_INPUT, 0, 0, 1, MUX2_A, MUX2_B, MUX2_C}, //pin 4 section 13
  {MUX2_INPUT, 1, 0, 1, MUX2_A, MUX2_B, MUX2_C}, 
  {MUX2_INPUT, 0, 1, 1, MUX2_A, MUX2_B, MUX2_C}, 
  {MUX2_INPUT, 1, 1, 1, MUX2_A, MUX2_B, MUX2_C},

  {MUX3_INPUT, 0, 0, 0, MUX3_A, MUX3_B, MUX3_C}, //pin 0 section 17
  {MUX3_INPUT, 1, 0, 0, MUX3_A, MUX3_B, MUX3_C}, 
  {MUX3_INPUT, 0, 1, 0, MUX3_A, MUX3_B, MUX3_C}, 
  {MUX3_INPUT, 1, 1, 0, MUX3_A, MUX3_B, MUX3_C}, 

  {MUX3_INPUT, 0, 0, 1, MUX3_A, MUX3_B, MUX3_C}, //pin 4 section 21
  {MUX3_INPUT, 1, 0, 1, MUX3_A, MUX3_B, MUX3_C}, 
  {MUX3_INPUT, 0, 1, 1, MUX3_A, MUX3_B, MUX3_C}, 
  {MUX3_INPUT, 1, 1, 1, MUX3_A, MUX3_B, MUX3_C},

  {-1, A0, 0, 0, 0, 0, 0},
  {-1, A1, 0, 0, 0, 0, 0},
  {-1, A2, 0, 0, 0, 0, 0},
  {-1, A3, 0, 0, 0, 0, 0},
  {-1, A4, 0, 0, 0, 0, 0},
  {-1, A5, 0, 0, 0, 0, 0},
  {-1, A6, 0, 0, 0, 0, 0},
  {-1, A7, 0, 0, 0, 0, 0},
  {-1, A8, 0, 0, 0, 0, 0},
  {-1, A9, 0, 0, 0, 0, 0},
 
  };

void setupMultiplexers() {
  pinMode(MUX1_INPUT, INPUT);
  pinMode(MUX1_A, OUTPUT);
  pinMode(MUX1_B, OUTPUT);
  pinMode(MUX1_C, OUTPUT);

  pinMode(MUX2_INPUT, INPUT);
  pinMode(MUX2_A, OUTPUT);
  pinMode(MUX2_B, OUTPUT);
  pinMode(MUX2_C, OUTPUT);

  pinMode(MUX3_INPUT, INPUT);
  pinMode(MUX3_A, OUTPUT);
  pinMode(MUX3_B, OUTPUT);
  pinMode(MUX3_C, OUTPUT);
}

void setup() {

  // Setup Serial Monitor
  Serial.begin(9600);
  setupMultiplexers();

  // Setup pins as Outputs
  pinMode(LED_LATCH_PIN, OUTPUT);
  pinMode(LED_CLOCK_PIN, OUTPUT);
  pinMode(LED_DATA_PIN, OUTPUT);
  
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);
  pinMode(A2, INPUT);
  pinMode(A3, INPUT);
  pinMode(A4, INPUT);
  pinMode(A5, INPUT);
  pinMode(A6, INPUT);
  pinMode(A7, INPUT);
  pinMode(A8, INPUT);
  pinMode(A9, INPUT);
}

int read_from_multiplexer(int mux_pin, int a_val, int b_val, int c_val, int a_pin, int b_pin, int c_pin) {
  // Serial.println(mux_pin);
  // Serial.println(a_pin);
  // Serial.println(b_pin);
  // Serial.println(c_pin);
  // Serial.println(a_val);
  // Serial.println(b_val);
  // Serial.println(c_val);
  digitalWrite(a_pin, a_val);
  digitalWrite(b_pin, b_val);
  digitalWrite(c_pin, c_val);
  delay(1);
  return analogRead(mux_pin);
}


bool isSectionCurrentlyPressed(byte section_index) {
  
  // if we're out of bounds here...
  if (section_index >= ( sizeof(section_to_input) / sizeof(section_to_input[0]) ) ) {
    // no need to make a big deal out of it, just say no
    return false;
  }
  
  // this will make it a bit easier to work with the input config we're working with
  int *input_config = section_to_input[section_index];
  
  // value we read
  // set to high by default not to get false positives if we code stuff wrong
  int val = 1000;
  // bool val_read = false;

  
  // that's an analog input, without the multiplexer
  if(input_config[0] == -1) {
    // reading directly
    val = analogRead(input_config[1]);

  // that means we're in multiplexer-land
  } else {
    // read through multiplexer
    val = read_from_multiplexer(input_config[0], input_config[1], input_config[2], input_config[3], input_config[4], input_config[5], input_config[6]);
  }

  // if we're below a threashold, we're the section is being "touched"
  // TODO: define some constant above for this:

  // if(val_read) {
  //   Serial.println("read value");
  //   Serial.println(val);
  // }

  if(val < 580) {
    return true;
  }
  
  // no touchy apparently
  return false;
}


void loop() {
    
//     Serial.println("section 1");
//     Serial.println(new_section_is_down(0));
//     Serial.println("section 2");
//     Serial.println(new_section_is_down(1));
//     Serial.println("section 3");
//     Serial.println(new_section_is_down(2));
//     Serial.println("section 4");
//     Serial.println(new_section_is_down(3));
// 
//     Serial.println("section 5");
//     Serial.println(new_section_is_down(4));
//     Serial.println("section 6");
//     Serial.println(new_section_is_down(5));
//     Serial.println("section 7");
//     Serial.println(new_section_is_down(6));
//     Serial.println("section 8");
//     Serial.println(new_section_is_down(7));
// 
//     Serial.println("section 9");
//     Serial.println(new_section_is_down(8));
//     Serial.println("section 10");
//     Serial.println(new_section_is_down(9));
//     Serial.println("section 11");
//     Serial.println(new_section_is_down(10));
//     Serial.println("section 12");
//     Serial.println(new_section_is_down(11));
// 
//     Serial.println("section 13");
//     Serial.println(new_section_is_down(12));
//     Serial.println("section 14");
//     Serial.println(new_section_is_down(13));
//     Serial.println("section 15");
//     Serial.println(new_section_is_down(14));
//     Serial.println("section 16");
//     Serial.println(new_section_is_down(15));
// 
//     Serial.println("");
//     Serial.println("");
//     delay(1000);

    // when is "now"?
    // 
    // no need to call millis() every time we need to figure out "now"
    // for this step of the loop
    unsigned long now = millis();
    
    // which leds should be lit?
    // assuming none for starters and working from there
    byte leds_lit[ NUM_REGISTERS ] = {
        0b00000000, 0b00000000, 0b00000000,
        0b00000000, 0b00000000, 0b00000000,
        0b00000000, 0b00000000, 0b00000000
    };
    
    // 
    // 1. loop through sections establishing their pressed status
    // 
    // this is where we handle (set, clear) the pressed status
    // and where we "start" new shakes
    // 
    // no LEDs are being lit or turned off here, that comes later
    // 
    
    // used to hold temporary state of a section
    bool currently_pressed = false;

    for (byte section = 0; section < 34; section++) {
        
        // is the section currently resed?
        currently_pressed = isSectionCurrentlyPressed(section);
        if (currently_pressed) {
          //Serial.print("pressed section: ");
          //Serial.println(section);
        }
        
        // was the section in pressed state last time over?
        if (sections_pressed[section]) {
            
            // if it's no longer pressed, we just clear the flag and we're done
            if (! currently_pressed) {
                sections_pressed[section] = false;
                //Serial.print("no longer pressed: ");
                //Serial.println(section);
            }
            // otherwise we have nothing else to do,
            // that particular earthquake has already been started
        
        // the section was not previously pressed
        } else {
            // if the section is now currently pressed
            // that's a new press! hurray, an earthquake!
            if (currently_pressed) {
                
                //Serial.print("newly pressed: ");
                //Serial.println(section);

                // record for posterity (next loop step, that is)
                sections_pressed[section] = true;
                
                // add to the list of shakes
                for (unsigned int i = 0; i < SHAKES_MAX; i++) {
                    // find an empty slot
                    if (shakes[i].next_step == 0) {
                        //Serial.print("new shake: ");
                        //Serial.println(i);
                        // schedule the next step
                        shakes[i].next_step = now + STEP_DELAY;
                        shakes[i].section_states[section] = SHAKE_SHAKING;
                        break;
                    }
                    // if no empty slot was found, oh well, whole lotta shaking going on
                }
                
            }
        }
        
    }
    
    // 
    // 2. loop through the shakes
    //
    // using these in the bowels of the code below
    byte neighbour = 0; 
    bool more_shakes = false;
    
    for (unsigned int i = 0; i < SHAKES_MAX; i++) {
        
        // first, do we need a state change?
        if (shakes[i].next_step > 0 && shakes[i].next_step < now) {
            
            //Serial.print("shake state change: ");
            //Serial.println(i);

            // clear the flag indicating that we have more shakes to come
            more_shakes = false;
            
            // all currently lit sections need to be set to having been lit already
            // all unlit neighbours of currently lit sections need to be set to lit
            for (byte section = 0; section < 34; section++) {
                if (shakes[i].section_states[section] == SHAKE_SHAKING) {
                    //Serial.print("already shaking: ");
                    //Serial.println(section);
                    // it shook already
                    shakes[i].section_states[section] = SHAKE_SHOOK;
                    // now the neighbours
                    for (byte j = 0; j < 8; j++) {
                        // reached the end of available neighbours?
                        if (sections_map[section].neighbours[j] == 255) {
                            break;
                        }
                        // neighbour we are currently considering
                        neighbour = sections_map[section].neighbours[j];
                        // is the neigbour non-lit?
                        if (shakes[i].section_states[neighbour] == SHAKE_NONE) {
                            //Serial.print("neighbour to be shaken: ");
                            //Serial.println(neighbour);
                            // so we do have more shakes coming!
                            more_shakes = true;
                            // flagging for later
                            // can't just set it to SHAKE_SHAKING as we are not done yet with this loop! 
                            shakes[i].section_states[neighbour] = SHAKE_WILL;
                        }
                    }
                }
            }

            // move all SHAKE_WILL to SHAKE_SHAKING state
            for (byte section = 0; section < 34; section++) {
              if (shakes[i].section_states[section] == SHAKE_WILL) {
                shakes[i].section_states[section] = SHAKE_SHAKING;
              }
            }
            
            // do we have more shakes coming?
            if (more_shakes) {
                shakes[i].next_step = now + STEP_DELAY;
                //Serial.print("more shakes coming for shake: ");
                //Serial.println(i);
            // no we don't, we can clean up this shake slot
            } else {
                shakes[i].next_step = 0;
                for (byte section = 0; section < 34; section++) {
                    shakes[i].section_states[section] = SHAKE_NONE;
                }
                //Serial.print("done with shakes: ");
                //Serial.println(i);
            }
            
            for (byte j = 0; j < 34; j++) {
              //Serial.print(shakes[i].section_states[j]);
              //Serial.print(' ');
            }
            //Serial.println();
        }
        
        // state change done, we can now use the data
        // to help assemble the shaking sections map
        // 
        // if this shake is at all relevant, that is
        if (shakes[i].next_step > 0) {
            
            // relevant it is; light up any sections that should be lit
            for (byte section = 0; section < 34; section++) {
                if (shakes[i].section_states[section] == SHAKE_SHAKING) {
                    for (int j = 0; j < NUM_REGISTERS; j++) {
                      leds_lit[j] |= sections_map[section].leds[j];
                    }
                }
            }
        }
    }
    
    //
    // 3. handle all the LEDs
    //
    setLedState(leds_lit);

    // wait a bit
    delay(MAIN_LOOP_DELAY);
    
}

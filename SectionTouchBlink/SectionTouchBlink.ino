#define NUM_REGISTERS 9

#define NUM_SQUARES 2;

byte inputs[] = {A0, A1};

// ST_CP pin 12
const int latchPin = 8;
// SH_CP pin 11
const int clockPin = 12;
// DS pin 14
const int dataPin = 11;

void setup() {
  // Setup pins as Outputs
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  // and as input
  for (byte j = 0; j < 2; j++) {
    pinMode(inputs[j], INPUT);
  }

  // Setup Serial Monitor
  //Serial.begin(9600);
}

void loop() {
  //Serial.println(lit);
  
  byte leds_lit[] = {
    0b00000000, 0b00000000, 0b00000000,
    0b00000000, 0b00000000, 0b00000000,
    0b00000000, 0b00000000, 0b00000000
  };

  if (analogRead(inputs[0]) < 500) {
    leds_lit[3] = 0b01111111;
  }

  if (analogRead(inputs[1]) < 500) {
    leds_lit[7] = 0b01111111;
  }

  // ST_CP LOW to keep LEDs from changing while reading serial data
  digitalWrite(latchPin, LOW);

  for (int i = NUM_REGISTERS - 1; i >= 0; i--) {
    shiftOut(dataPin, clockPin, MSBFIRST, leds_lit[i]);
  }

  // ST_CP HIGH change LEDs
  digitalWrite(latchPin, HIGH);
  delay(50);

}
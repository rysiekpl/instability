#define NUM_REGISTERS 9

// ST_CP pin 12
const int latchPin = 8;
// SH_CP pin 11
const int clockPin = 12;
// DS pin 14
const int dataPin = 11;

int lit = 0;

void setup() {
  // Setup pins as Outputs
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  // Setup Serial Monitor
  //Serial.begin(9600);
}

void loop() {
  //Serial.println(lit);

  // Shift out the bits
  for (byte j=1; j<128; j<<=1) {

    // ST_CP LOW to keep LEDs from changing while reading serial data
    digitalWrite(latchPin, LOW);

    for (int i = NUM_REGISTERS - 1; i >= 0; i--) {
      if (i == lit) {
        shiftOut(dataPin, clockPin, MSBFIRST, j);
      } else {
        shiftOut(dataPin, clockPin, MSBFIRST, 0);
      }
    }

    // ST_CP HIGH change LEDs
    digitalWrite(latchPin, HIGH);
    delay(50);
    
  }

  lit++;
  if (lit >= NUM_REGISTERS) {
    lit = 0;
  }
}